{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# HTW Berlin - Angewandte Informatik - Advanced Topics - Exercise - Monte Carlo Estimator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Modules](#Python-Modules)\n",
    "* [Importance of Sampling](#Importance-of-Sampling)\n",
    "* [Exercise - Monte Carlo Estimator](#Exercise---Monte-Carlo-Estimator)\n",
    "  * [Exercise - Inverse Transform Sampling](#Exercise---Inverse-Transform-Sampling)\n",
    "  * [Exercise - Empirical Variance](#Exercise---Empirical-Variance)\n",
    "  * [Exercise - Variance Reduction by Control Variates](#Exercise---Variance-Reduction-by-Control-Variates)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook you will first see why it is important to draw samples according to the probability distribution and not just using a grid (`np.linspace(...)`) or a uniform distribution. Afterwards, you will implement inverse transform sampling and use it to estimate a function with respect to a probability density function. Finally, you will make use of control variates to reduce the variance of the estimator and to improve the estimation.\n",
    "\n",
    "In order to detect errors in your own code, execute the notebook cells containing `assert` or `assert_almost_equal`. These statements raise exceptions, as long as the calculated result is not yet correct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "To complete this exercise notebook, you should possess knowledge about the following topics.\n",
    "* Probability density function\n",
    "* Probability mass function\n",
    "* Expected value (**Exercise - Expected Value**)\n",
    "* Monte Carlo estimator \n",
    "* Inverse transform sampling\n",
    "* Variance reduction\n",
    "\n",
    "The following material can help you to acquire this knowledge:\n",
    "* Read Chapter 3 \"Probability and Information Theory\" of the [Deep Learning Book](http://www.deeplearningbook.org/)\n",
    "* https://www.youtube.com/watch?v=irheiVXJRm8 (Smirnov Transform)\n",
    "* https://www.math.nyu.edu/faculty/goodman/teaching/MonteCarlo2005/notes/VarianceReduction.pdf (Variance reduction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from numpy.testing import assert_almost_equal\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def plot_p_f_and_samples(x, p, f, samples_x1=[], samples_x2=[]):\n",
    "    \"\"\" \n",
    "    Plots two functions p(x) and f(x) for given x values\n",
    "    and f(samples_x) as single points\n",
    "    \n",
    "    :param x: x-values for the functions p and f\n",
    "    :type x: 1D ndarray\n",
    "    :param p: pdf\n",
    "    :type p: lambda function\n",
    "    :param f: function evaluated with x and samples_x\n",
    "    :type f: lambda function\n",
    "    :param samples x: x-values for the function f drawn as single points    \n",
    "    \"\"\"\n",
    "    plt.plot(x, p(x), label='p(x)')\n",
    "    plt.plot(x, f(x),label='f(x)')\n",
    "    if len(samples_x1) > 0:\n",
    "        plt.plot([samples_x1[:-1]], [f(samples_x1)[:-1]], marker='x', color='g')\n",
    "        plt.plot([samples_x1[-1]], [f(samples_x1)[-1]], marker='x', color='g', label='f(samples_x1)')\n",
    "    if len(samples_x2) > 0:\n",
    "        plt.plot([samples_x2[:-1]], [f(samples_x2)[:-1]], marker='x', color='r')\n",
    "        plt.plot([samples_x2[-1]], [f(samples_x2)[-1]], marker='x', color='r', label='f(samples_x2)')\n",
    "    plt.xlabel('x')\n",
    "    plt.legend(loc='upper left')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Crude Monte Carlo Estimator (CMC)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We have a function, which estimates $\\mathbb E_{x \\sim p} [f(x)]$ with the use of $n$ drawn samples, with the given functions: \n",
    "\n",
    "$$\n",
    "f(x)=\\sin(x)\n",
    "$$ \n",
    "\n",
    "and \n",
    "$$\n",
    "p(x) = \\text{uniform}(0, \\pi/2)\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Variance Reduction by Control Variates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "Determine the empirical variance (also sample variance) of the CMC-estimator for n = 10 by computing the expected value 10,000 times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Complete this cell\n",
    "#(...)\n",
    "\n",
    "#(...)\n",
    "e_variance = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Executing this cell must not throw an Exception\n",
    "# The solution is obfuscated so you can solve the exercise without unintendedly spoiling yourself\n",
    "\n",
    "#obfuscated_solution = 2366.531932907348 * 626 / 72727\n",
    "#assert_almost_equal(e_variance, obfuscated_solution, decimal=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The following plot visualizes how the estimation converges towards the true expected value as n increases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# PLOT\n",
    "\n",
    "n=500\n",
    "x_, fs_mean, fs = estimate_f_CMC(n=n)\n",
    "x = np.arange(n-1)\n",
    "e = np.ndarray(n-1)\n",
    "for i in range(1,n):\n",
    "    e[i-1] = fs[:i].mean()\n",
    "    \n",
    "print(fs.mean())\n",
    "\n",
    "plt.figure(figsize=(20,4))\n",
    "plt.plot(x,e)\n",
    "#plt.axhline(y=2., c='g')\n",
    "plt.xlabel(\"num of samples\")\n",
    "plt.ylabel(\"estimate\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Variance Reduction by Control Variates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "- Reduce the empirical variance and improve the estimator with the use of control variates.\n",
    "- What's the error (empirical standard deviation) of the estimation?\n",
    "\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "We have\n",
    "- $\\hat F$ is CMC the estimator of $\\mathbb E_p [f(x)]$:\n",
    "$$\n",
    "\\hat F = \\frac{1}{n}\\sum_i^n f(x^{(i)})\n",
    "$$\n",
    "\n",
    "Search for a variable $g(x)$, which fulfills the following:\n",
    "\n",
    "* $g(x)$ is correlated with $f(x)$\n",
    "* its expected value $\\mathbb E[g(X)] $ is known\n",
    "\n",
    "**Reminder:**\n",
    "\n",
    "$\\bar F$ is the improved estimated value \n",
    "\n",
    "$$\n",
    "\\bar F = \\hat F - \\beta \\cdot ( G - \\mathbb E[g(X)] )\n",
    "$$\n",
    "\n",
    "with \n",
    "$$\n",
    "G = \\frac{1}{n}\\sum_i^n g(x^{(i)})\n",
    "$$\n",
    "with optimum $\\beta$:\n",
    "\n",
    "$$\n",
    "\\beta = \\frac{cov(\\hat F,\\bar F)}{var(\\bar F)}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Complete this cell\n",
    "\n",
    "# get some values for V and W \n",
    "x_, fs_mean, fs =  estimate_f_CMC(n=10)\n",
    "\n",
    "# (...)\n",
    "\n",
    "b = None# calculate beta first"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Executing this cell must not throw an Exception\n",
    "# The solution is obfuscated, so you can solve the exercise without unintendedly spoiling yourself\n",
    "\n",
    "#obfuscated_solution = TODO\n",
    "#assert_almost_equal(b,obfuscated_solution)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Now calculate the improved estimation of f\n",
    "\n",
    "# (...)\n",
    "\n",
    "Z = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Executing this cell must not throw an Exception\n",
    "\n",
    "solution = .636\n",
    "assert_almost_equal(cmc_estimates.mean(), solution, decimal=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"KLE\"></a>[KLE]\n",
    "        </td>\n",
    "        <td>\n",
    "            Jack P. C. Kleijnen, Ad A. N. Ridder, and Reuven Y. Rubinstein: Variance Reduction Techniques in Monte\n",
    "Carlo Methods\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "HTW Berlin - Angewandte Informatik - Advanced Topics - Exercise - Monte Carlo Estimator <br/>\n",
    "by Christian Herta, Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Christian Herta, Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
