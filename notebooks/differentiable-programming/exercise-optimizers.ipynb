{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimizers (Differentiable Programming)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements)\n",
    "* [Python Modules](#Python-Modules)\n",
    "* [Data](#Data)\n",
    "* [Exercises](#Exercises)\n",
    "    * [Motivation: Exponential moving average](#Motivation:-Exponential-moving-average)\n",
    "    * [Bias correction](#Bias-correction)\n",
    "    * [Momentum](#Momentum)\n",
    "    * [RMSProp](#RMSProp)\n",
    "    * [Adam](#Adam)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "At this point, you are probably very familiar with vanilla gradient descent optimization and its update rule\n",
    "```python\n",
    "weights = weights - alpha * gradients.\n",
    "```\n",
    "In this Notebook, you will implement three types of optmization algorithms that aim to improve upon classic gradient descent: **SGD + Momentum**, **RMSProp** and the **Adam** optimizers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Prerequisites\n",
    "This notebook works with the neural net framework you've been building in the 'Differentiable Programming' course.\n",
    "\n",
    "### Knowledege\n",
    "The algorithms you'll implement in this notebooks are described in the following videos by deeplearning.ai/Andrew NG:\n",
    "* [Exponentially Weighted Averages (C2W2L03)](https://youtu.be/lAq96T8FkTw)\n",
    "* [Bias Correction of Exponentially Weighted Averages (C2W2L05)](https://youtu.be/lWzo8CajF5s)\n",
    "* [Gradient Descent With Momentum (C2W2L06)](https://youtu.be/k8fTYJPd3_I)\n",
    "* [RMSProp (C2W2L07)](https://youtu.be/_e-LFe_igno)\n",
    "* [Adam Optimization Algorithm (C2W2L08)](https://www.youtube.com/watch?v=JXQT_vxqwIs&list=PLkDaE6sCZn6Hn0vK8co82zjQtt3T2Nkqc&index=22)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from dp import Optimizer,Model,Node,SGD\n",
    "from sklearn import datasets,preprocessing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "This cell downloads the [breast cancer dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html#) provided by [sklearn](https://scikit-learn.org/stable/index.html) and defines a model for the classification task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_train,y_train = datasets.load_breast_cancer(return_X_y=True)\n",
    "x_train = preprocessing.scale(x_train)\n",
    "\n",
    "class Net(Model):    \n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        self.h = self.Linear_Layer(30, 20, \"h0\")\n",
    "        self.h2 = self.Linear_Layer(20, 10, \"h1\")\n",
    "        self.h3 = self.Linear_Layer(10, 1, \"h2\")\n",
    "        \n",
    "    def loss(self, x, y):\n",
    "        if not type(y) == Node:\n",
    "            y = Node(y)\n",
    "        out = self.forward(x)\n",
    "        loss = -1 * (y * out.log() + (1 - y) * (1 - out).log())\n",
    "        return loss.sum()\n",
    "    \n",
    "    def forward(self, x):\n",
    "        if not type(x) == Node:\n",
    "            x = Node(x)\n",
    "        x = self.h(x).tanh()\n",
    "        x = self.h2(x).leaky_relu()\n",
    "        out = self.h3(x).sigmoid()\n",
    "        \n",
    "        return out\n",
    "    \n",
    "def train_breast_cancer(optimizer):\n",
    "    net = Net()\n",
    "    optim = optimizer(net,x_train,y_train)\n",
    "    loss,loss_hist,para_hist = optim.train(steps=1000,print_each=100)\n",
    "    plt.plot(np.arange(len(loss_hist)),loss_hist, label=optimizer.__name__)\n",
    "    plt.xlabel('iterations')\n",
    "    plt.ylabel('loss')\n",
    "    plt.legend()\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "### Motivation: Exponential moving average\n",
    "\n",
    "The moving average is a way to smooth out a noisy signal by averaging over the history of values. The further a data point lies in the past, the lower its influence on the current moving average. \n",
    "\n",
    "The example below shows a noisy signal. The x values are the interval $[-\\pi \\dots \\pi)$. The y-values are $cos(x)$ plus random noise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(-np.pi,np.pi,200)\n",
    "y = np.cos(x)\n",
    "target = y + np.random.uniform(low=-2,high=2,size=x.shape) * 0.2\n",
    "plt.plot(x,target,'rx',label='true noisy data')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you were to draw a line through each point precisely, you would create a line with many oscillations in the vertical direction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plt.plot(x,target,'r')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The series of exponential moving averages can be computed recursively:\n",
    "* original signal: $\\theta_{1:T} = [ \\theta_1, \\theta_2 \\dots \\theta_T ]$\n",
    "* exponential moving average:\n",
    "\n",
    "  $s_0$ = 0\n",
    "  \n",
    "  $s_t = \\beta * s_{t-1} + (1 - \\beta) * \\theta_t$\n",
    " \n",
    "Beta $\\beta$ is a hyperparameter in the interval [0..1]. Note that the initial value $s_0$ is not part of the generated series, it just provides an initial value to plug into the calculation for $s_1$.\n",
    "\n",
    "The aim of the moving average is to smoothen out the original noisy signal. You can also say the moving average dampens the oscillations in the horizontal direction.\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Implement a function to return the smoothed series of moving averages for a signal. (You can ignore the parameter `bias_correction` for now) Try different values for beta, e.g. **0.1**, **0.9** and **0.999** and note the changes in the curve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gen_smooth(signal,beta,bias_correction=False):\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the original noisy series and the series of exponentially moving averages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "beta = 0.9\n",
    "smooth_target = list(gen_smooth(target,beta))\n",
    "plt.plot(x,target,'rx',label='true noisy data')\n",
    "plt.plot(x,smooth_target,label='smoothed data')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bias correction\n",
    "\n",
    "With the graph we can spot a flaw with exponential moving averages: The smoothed series starts at 0 while the true series starts around -1.This error propagates through the series and its effect is especially visible early in the series.\n",
    "\n",
    "With bias correction we can get better estimates early on. Instead of returning the value $s_t$ directly, we return $s_t^{corrected}$ as\n",
    "\n",
    "```python\n",
    "s_corrected = s/(1 - beta**t)\n",
    "```\n",
    "**Task:**\n",
    "\n",
    "Update your implementation to perform bias correction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gen_smooth(signal,beta,bias_correction=False):\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "beta = 0.9\n",
    "smooth_target = list(gen_smooth(target,beta,bias_correction=True))\n",
    "plt.plot(x,target,'rx',label='true noisy data')\n",
    "plt.plot(x,smooth_target,label='smoothed data')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Momentum\n",
    "\n",
    "We can apply the concept of moving averages to gradient descent optimization. In each iteration, we calculate the gradient over a mini-batch of samples. The idea is to treat these gradients as noisy and flatten out the oscillations. In vanilla gradient descent, you use these gradients for the update directly e.g.\n",
    "```python\n",
    "w = w - alpha * dW.\n",
    "```\n",
    "\n",
    "\n",
    "In momentum, you compute the gradient dW as usual. *Then*, you update the moving average of the gradient. In this algorithm the moving average is called **velocity**.\n",
    "Then you shift the parameters by the negative velocity scaled by the learning rate, as opposed to the gradient itself.\n",
    "\n",
    "In pseudo-code:\n",
    "\n",
    "``` python\n",
    "vdW = np.zeros_like(w) # velocity of weight gradients, starts at 0\n",
    "vdB = np.zeros_like(b) # velocity of bias gradients, starts at 0\n",
    "for i in range(steps):\n",
    "    dW,db = calc_gradients()\n",
    "    vdW = beta * vdW + (1 - beta) * dW # update velocities\n",
    "    vdb = beta * vdb + (1 - beta) * db\n",
    "    w = w - alpha * vdW # update weights\n",
    "    b = b - alpha * vdb\n",
    "```\n",
    "\n",
    "So now there are two hyperparameters:\n",
    "* alpha, the learning rate\n",
    "* beta, the hyperparameter used for velocity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Reference implementation:** For reference, this is the vanilla SGD optimizer implemented in the neural net framework. Refer to the comments about how the update is performed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SGD(Optimizer):\n",
    "    \n",
    "    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):\n",
    "        # call parent constructor\n",
    "        super(SGD, self).__init__(model, x_train, y_train, hyperparam, batch_size)\n",
    "        \n",
    "    def _set_param(self):\n",
    "        # set hyperparameters\n",
    "        self.alpha = self.hyperparam.get(\"alpha\", 0.001)\n",
    "\n",
    "    def _update(self, param, grad, g, i):\n",
    "        # param - A dictionary of parameters in the network\n",
    "        # grad - dict; gradients calculated in this epoch\n",
    "        # param - dict; parameters of the network\n",
    "        # g - name of the parameters\n",
    "        \n",
    "        # update the network:\n",
    "        update = param[g] - self.alpha * grad[g]  \n",
    "        np.copyto(param[g], update)\n",
    "             \n",
    "    def train(self, steps=1000, print_each=100):\n",
    "        # num_grad_stores creates copies of dictionaries { k : v }\n",
    "        #   k : str -  is the name of the parameter in the network.\n",
    "        #   v : np.ndarray - has the same shape as the parameter\n",
    "        #       and is initialized at zeros.\n",
    "        # for example in momentum, you can use self.grad_stores[0]\n",
    "        # to keep track of the velocity.\n",
    "        return self._train(steps, num_grad_stores=1, print_each=print_each)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task:**\n",
    "\n",
    "Implement the Momentum optimizer. You can use self.grad_stores[0] to keep track of the velocity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SGD_Momentum(Optimizer):\n",
    "    \n",
    "    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):\n",
    "        super(SGD_Momentum, self).__init__(model, x_train, y_train, hyperparam, batch_size)\n",
    "        \n",
    "    def _set_param(self):\n",
    "        raise NotImplementedError() #TODO: set hyperparameters self.alpha and self.beta\n",
    "    \n",
    "    def _update(self, param, grad, g, i):\n",
    "        velocity = self.grad_stores[0]\n",
    "        v = np.zeros_like(grad[g]) # TODO: update velocity\n",
    "        np.copyto(velocity[g], v)\n",
    "        \n",
    "        # TODO: update network\n",
    "        update = np.zeros_like(grad[g])\n",
    "        np.copyto(param[g], update)\n",
    "    \n",
    "    def train(self, steps=1000, print_each=100):\n",
    "        return self._train(steps, num_grad_stores=1, print_each=print_each)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the cell below to train the breast cancer model with this optimizer and plot the loss."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "train_breast_cancer(SGD_Momentum)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### RMSProp\n",
    "\n",
    "RMSProp keeps track of the moving average **of the squared** gradients, rather than the gradients themselves.\n",
    "\n",
    "sdW is the running average of the squared weight gradients, the bias is left out from this example for brevity.\n",
    "\n",
    "In the update, the gradients are divided by the square root of the moving average of squared gradients.\n",
    "\n",
    "``` python\n",
    "sdW = np.zeros_like(w)\n",
    "for i in range(steps):\n",
    "    dW = calc_gradients()\n",
    "    sdW = beta * sdW + (1 - beta) * np.square(dW) # update running avg. of square\n",
    "    w = w - alpha * dW/np.sqrt(sdW + epsilon) # update rule\n",
    "    \n",
    "```\n",
    "\n",
    "Why is this done?\n",
    "* sdW is expected to be small. So you divide dW by a small number and provide a boost to the weight update.\n",
    "* sdb is expected to be large. So you divide db by a large number and dampen the bias update.\n",
    "\n",
    "So now there are three hyperparameters:\n",
    "* alpha, the learning rate. There is no default value, experiment with it and see what works.\n",
    "* beta2, used to update the moving average of the squared gradients. A common default value is **0.99**. We call this beta2 to distinguish it from the parameter beta used in momentum.\n",
    "* epsilon - An arbitrary small number added to the root in the denominator. It prevents that we divide by zero or a number so small that the fraction explodes. A common default value is **10e-8**\n",
    "\n",
    "**Task:** \n",
    "\n",
    "Implement the RMSProp optimizer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class RMS_Prop(Optimizer):\n",
    "    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):\n",
    "        super(RMS_Prop, self).__init__(model, x_train, y_train, hyperparam, batch_size)\n",
    "        \n",
    "    def _set_param(self):\n",
    "        # TODO: set self.alpha, self.beta2, self.epsilon\n",
    "        raise NotImplementedError()\n",
    "        \n",
    "    def _update(self, param, grad, g, i):\n",
    "        # TODO: update the moving avg. of squared gradients and the network\n",
    "        raise NotImplementedError()\n",
    "        \n",
    "    def train(self, steps=1000, print_each=100):\n",
    "        return self._train(steps, num_grad_stores=1, print_each=print_each)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_breast_cancer(RMS_Prop)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adam\n",
    "Adam combines the properties of both Momentum and RMSProp. It keeps track of the **velocity** as the moving average of the gradients. It also keeps track of the moving average of the **squared gradients**. Additionally, we apply bias correction to the velocity and avg. of squared gradients.\n",
    "\n",
    "In the update rule, we shift the parameter by the negative **velocity** divided by the **square root of the avg.** of squared gradients. Using the variable names from the previous exercies:\n",
    "```python\n",
    "w = w - alpha * vdW_corrected/np.sqrt(sdW_corrected)\n",
    "```\n",
    "\n",
    "So now there are 4 hyperparameters:\n",
    "  * alpha, the learning rate\n",
    "  * beta, the hyperparameter for the moving average of the velocity. A common default value is 0.9\n",
    "  * beta2, the hyperparameter for the moving average of the squared gradients. A common default value is 0.99\n",
    "  * epsilon : a small value added to the denominator. Common default value: 10e-8\n",
    "  \n",
    "**Task:** Implement the Adam optimizer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Adam(Optimizer):\n",
    "    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):\n",
    "        super(Adam, self).__init__(model, x_train, y_train, hyperparam, batch_size)\n",
    "        \n",
    "    def _set_param(self):\n",
    "        # TODO: set alpha, beta, beta2, epsilon\n",
    "        raise NotImplementedError()\n",
    "    \n",
    "    def _update(self, param, grad, g, i):\n",
    "        # TODO: update the velocity, moving avg. of squared gradients and network\n",
    "        raise NotImplementedError()\n",
    "    \n",
    "    def train(self, steps=1000, print_each=100):\n",
    "        # note that num_grad_stores is 2 since you need to keep track\n",
    "        # of both the velocity and moving avg. of squared gradients\n",
    "        return self._train(steps, num_grad_stores=2, print_each=print_each)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_breast_cancer(Adam)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"NG17-1\"></a>[NG17-1]\n",
    "        </td>\n",
    "        <td>deeplearning.ai/Andrew Ng. \"Exponentially Weighted Averages (C2W2L03).\" 25 Aug 2017 [online] Available at: https://www.youtube.com/watch?v=lAq96T8FkTw [Accessed 8 Jun. 2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"NG17-2\"></a>[NG17-2]\n",
    "        </td>\n",
    "        <td>deeplearning.ai/Andrew Ng. \"Bias Correction of Exponentially Weighted Averages (C2W2L05)\" 25 Aug 2017 [online] Available at: https://www.youtube.com/watch?v=lWzo8CajF5s [Accessed 8 Jun. 2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"NG17-3\"></a>[NG17-3]\n",
    "        </td>\n",
    "        <td>deeplearning.ai/Andrew Ng. \"Gradient Descent With Momentum (C2W2L06)\" 25 Aug 2017 [online] Available at: https://www.youtube.com/watch?v=k8fTYJPd3_I [Accessed 8 Jun. 2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"NG17-4\"></a>[NG17-4]\n",
    "        </td>\n",
    "        <td>deeplearning.ai/Andrew Ng. \"RMSProp (C2W2L07)\" 25 Aug 2017 [online] Available at: https://www.youtube.com/watch?v=_e-LFe_igno [Accessed 8 Jun. 2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"NG17-5\"></a>[NG17-5]\n",
    "        </td>\n",
    "        <td>deeplearning.ai/Andrew Ng. \"Adam Optimization Algorithm (C2W2L08)\" 25 Aug 2017 [online] Available at: https://www.youtube.com/watch?v=_e-LFe_igno [Accessed 8 Jun. 2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"BUS17\"></a>[BUS17]\n",
    "        </td>\n",
    "        <td>Vitaly Bushaev. \"Stochastic Gradient Descent with momentum\" 4 Dec 2017 [online] Available at: https://towardsdatascience.com/stochastic-gradient-descent-with-momentum-a84097641a5d [Accessed 8 Jun. 2019].\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "_Optimizers_ <br/>\n",
    "by _Diyar Oktay_ <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 _Diyar Oktay_\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
